![mad_AdNetworkMediation_rgb_small.png](https://bitbucket.org/repo/GyRXRR/images/3981639300-mad_AdNetworkMediation_rgb_small.png) for Cordova iOS and Android

You can see explanation for native [MngAds Ios] or [MngAds Android] SDKs. Our single [mng-ads-sdk-cordova-plugin] can be used for Ios and Android Apps

[TOC]
## Install with the Cordova CLI

Can you please Prerequisites and Installing procedure of [Cordova CLI].

### Create the App
```
#!unix
$ sudo npm install -g cordova
$ cordova create mngAdsCordovaDemo com.cordova.mngads mngAdsCordovaDemo
$ cd mngAdsCordovaDemo
```

### Add Platforms

```
#!unix

$ cordova platform add android
$ cordova platform add ios
$ cordova platforms ls
```

### Add Plugins

You must download or clone [mng-ads-sdk-cordova-plugin] repository. Then add it to your project.

```
#!unix

$ cordova plugin add ../mng-ads-sdk-cordova-plugin
```

On previous version < cordova 5, you can add a plugin from repo


```
#!unix
$ cordova plugin add https://bitbucket.org/mngcorp/mngads-cordova-plugin.git#/mng-ads-sdk-cordova-plugin 
```

### iOS9 fixes
Before building your application, you have to open the xcode project on `platforms/ios/YOUR_APP.xcodeproj` and set:
- Build settings -> bitcode enabled to *NO* 
- General -> deployment target iOS 7+

### Use our www demo

You find an example of implementation on [www] directory

### Implementation

#### Initializing the SDK

You must set the appId provided by mngAds team.

```
#!javascript
//set appId
mngadsAppId: function() {
return (device.platform == "iOS")?'YOUR_IOS_APPID':'YOUR_ANDROID_APPID';
},
//Create MngAds Object
initMngAds: function() {
// Set your AppId.
this.mngAds = new MngAdsSDK();
if (! this.mngAds ) { alert( 'mngAds plugin not ready' ); return; }
// Connection to the API.
this.mngAds.initWithAppId(this.mngadsAppId());
this.createInterstitial();
},
```


#### Banner

##### Make a request

You must set the placementId provided by mngAds team.

```
#!javascript

/**
* Create banner.
*  @param: placementId
*  @param: height: requested height (dp for android and pt for iOS)
*  @param: position: TOP or BOTTOM
*  @param: autoDisplay: if autoDisplay == false, use MngAdsSDK.prototype.showBanner to show it
*  @param: preferences: (gender, location, keyword...)
*  @param: successCallback: this callback is called when banner did load
*  @param: failureCallback: this callback is called when Factory failed to create banner (isBusy,worong placmentID,No ad,Timeout ...)
*/
createBanner: function(id,size,position) {
var preferences = "{\
\"age\": \"25\",\
\"language\": \"fr\",\
\"keyword\": \"brand=myBrand;category=sport\",\
\"gender\": \"M\",\
\"location\": {\
\"lat\": \"48.876\",\
\"lon\": \"10.453\"\
}\
}";
document.getElementById("status-ads").innerHTML="waiting "+device.platform;
// Set your placementId.
var placementId = '/'+this.mngadsAppId()+'/homebanner';
this.mngAds.createBanner(placementId,size,position,true,preferences,this.adSuccess,this.adError)
},
```





#### Interstitial

##### Make a request

You must set the placementId provided by mngAds team.

```
#!javascript

/**
* Create interstitial.
*  @param: placementId
*  @param: preferences: (gender, location, keyword...)
*  @param: successCallback: this callback is called when interstitial did load or did disappear
*  @param: failureCallback: this callback is called when Factory failed to create interstitial (isBusy,worong placmentID,No ad,Timeout ...)
*  @param: autoDisplay: to chose if the interstitial will be displayed automatically
*/
createInterstitial: function(autoDisplay) {
var preferences = "{\
\"age\": \"25\",\
\"language\": \"fr\",\
\"keyword\": \"brand=myBrand;category=sport\",\
\"gender\": \"M\",\
\"location\": {\
\"lat\": \"48.876\",\
\"lon\": \"10.453\"\
}\
}";
document.getElementById("status-ads").innerHTML="waiting "+device.platform;
// Set your placementId.
var placementId = '/'+this.mngadsAppId()+'/interstitial';
this.mngAds.createInterstitial(placementId,preferences,this.adSuccess,this.adError,autoDisplay);

},
```

##### Show interstitial

If you create an interstitial with autoDisplay = false, you have to call showInterstitial

```
#!javascript
/**
* Show interstitial.
*  @param: successCallback: this callback is called when interstitial showen
*  @param: failureCallback: this callback is called when Factory failed to show interstitial
*/

showInterstitial: function() {
document.getElementById("status-ads").innerHTML="waiting Interstitial " ;
this.mngAds.showInterstitial(function(message)
{
document.getElementById("status-ads").innerHTML="Interstitial displayed" ;
},
function(message) {
document.getElementById("status-ads").innerHTML="Fail to display " ;
});

},

```

#### Native Ad
##### Make request

You must set the placementId provided by mngAds team.

```
#!javascript
/**
* Create native.
*  @param: placementId
*  @param: preferences: (gender, location, keyword...)
*  @param: successCallback: this callback is called when native Ad did load
*  @param: failureCallback: this callback is called when Factory failed to create native Ad (isBusy,worong placmentID,No ad,Timeout ...)
*/
createNative: function() {
var age = "25";
var language = "fr";
var keyword="brand=myBrand;category=sport;";
var gender="M";
var location={"lat":48.876,"lon":10.453};
var preferences = {
"age": age,
"language": language, 
"keyword":keyword,
"gender":gender,
"location":location
}
// Set your placementId.
var placementId = '/'+this.mngadsAppId()+'/nativead';
document.getElementById("status-ads").innerHTML="waiting Native "+placementId ;
this.mngAds.createNative(placementId,preferences,this.nativeAdSuccess,this.adError)
}, 
```

The native ad return the meta data of the ad as a json

```
#!javascript
nativeAdSuccess: function(nativeObject) {

document.getElementById("status-ads").innerHTML="success - didLoad";
var nativeObjectArray = JSON.parse(nativeObject);

// NATIVE_TITLE : nativeObjectArray.title
// NATIVE_BODY  : nativeObjectArray.body
// NATIVE_SOCIAL_CONTEXT : nativeObjectArray.social_context
// NATIVE_CALL_TO_ACTION : nativeObjectArray.call_to_action
// NATIVE_AD_ICON : nativeObjectArray.ad_icon_url
// NATIVE_AD_COVER_IMAGE : nativeObjectArray.ad_cover_url
// NATIVE_BADGE_URL : nativeObjectArray.badge_url

var nativeAdContent =' <table style="width:100%;margin: 0 0;"><tr> <td><img style="width:50px;height:50px;margin: 0 0;" src='+nativeObjectArray.ad_icon_url+'></td> <td><h4 style ="color:red;line-height: 90%;margin: 0 0;">'+nativeObjectArray.title + '</h4></td> <td valign="top"><img src="'+nativeObjectArray.badge_url+'"style="width:50px;height:15px align="right"></td> </tr></table><h6 style ="margin: 0 0;">'+nativeObjectArray.body+'</h6><div id="mediaContainer" style="height: 200px; width: 100%; border: 1px    solid black;"></div><button id="NativeClick" class="button button-block button-rounded button-ads button-large">'+nativeObjectArray.call_to_action+'</button>';
};
```

##### Update click area
To support native click you have to send the position of the click area

```
#!javascript
updateClickArea :function (){
// change the click area
var elem = document.getElementById('NativeClick'),
properties = window.getComputedStyle(elem, null),           
x=elem.offsetLeft-document.body.scrollLeft;
y=elem.offsetTop-document.body.scrollTop;     
w = parseInt(properties.width, 10);
h = parseInt(properties.height, 10);
this.mngAds.setNativeAdClickArea(x, y, w, h);

}
```

##### Update media container area
To add the native media container you have to send the position of the media container area

```
#!javascript
updateMediaContainerArea :function(){
var elem = document.getElementById('mediaContainer'),
properties = window.getComputedStyle(elem, null),           
x=elem.offsetLeft-document.body.scrollLeft;
y=elem.offsetTop-document.body.scrollTop;     
w = parseInt(properties.width, 10);
h = parseInt(properties.height, 10);
this.mngAds.setMediaContainerArea(x, y, w, h);
}
```

You have to call setMediaContainerArea and setNativeAdClickArea when the position of the native div changed (for example at scroll)

```
#!javascript
window.addEventListener( "scroll", function( event ) {
app.updateClickArea();
app.updateMediaContainerArea();
});
```

##### Remove native ad
When you want to remove native ad div you have to call removeNativeAd which will remove the native media view and click view

```
#!javascript
/**
* remove native Ad.
* @param: successCallback: this callback is called when native ad is removed
* @param: failureCallback: this callback is called when sdk cant remove native
*/
removeNative: function() {

this.mngAds.removeNative(this.adSuccess,this.adError);

},
```

#### Custom adNetwork Mediation

To remove a specific ad network for specific paltform (ios or android), you must remove (or comment) the relative section on [plugin.xml] before adding it.

**example:** to remove smart ad server you have to comment those sections
```xml
        <!-- Smart Ad Server -->
            <!-- SAS Static Libraries -->
            <source-file src="src/ios/libSmartAdServer.a" framework="true" />
            <source-file src="src/ios/libMNGAdsSASAdapter.a" framework="true" />
            <!-- SAS Bundle -->
            <resource-file src="src/ios/sas.bundle"/>
        <!--/ Smart Ad Server -->
```
```xml
<!-- smart config-->
    <source-file src="src/android/SmartAdServer-Android-SDK-6.5.jar" target-dir="libs" framework="true" />
<!-- end smart config-->
```


### Build the App


```
#!unix

$ cordova build ios
$ cordova build android
```

### Build with ionic and android-23

```
#!unix
ionic start myApp blank
ionic platform add android@5.0.0 // try targetbuild android-23
ionic plugin add ../mng-ads-sdk-cordova-plugin
ionic run android
```

## Troubleshooting

#### If another plugin used

```
#!xml
<framework src="com.google.android.gmslay-services-analytics:+" />
```
You must remove following line in our [plugin.xml]
```
#!xml
<framework src="com.google.android.gmslay-services-analytics:+" />
```

#### If you come a cross this build error :

```
* What went wrong:
Execution failed for task ':processDebugManifest'.
> Manifest merger failed : uses-sdk:minSdkVersion 16 cannot be smaller than version 18 declared in library [:b4s-android-sdk-playservices830:] /home/Bureau/mngAdsIonicDemo/platforms/android/build/intermediates/exploded-aar/b4s-android-sdk-playservices830/AndroidManifest.xml
  	Suggestion: use tools:overrideLibrary="com.ezeeworld.b4s.android.sdk.playservices" to force usage

```

you have to manually edit you manifest.xml, under platforms/android :

change your manifest tag by adding tools xmlns :
```
#!xml
<manifest android:hardwareAccelerated="true" android:versionCode="1" android:versionName="0.0.1" package="com.ionicframework.mngadsdemo" xmlns:android="http://schemas.android.com/apk/res/android" xmlns:tools="http://schemas.android.com/tools">
.
.
.
</manifest>
```
 ignore the B4S minimum SDK level by adding
```
    <uses-sdk tools:overrideLibrary="com.ezeeworld.b4s.android.sdk.playservices"/>
```

[MngAds Ios]:https://bitbucket.org/mngcorp/mngads-demo-ios
[MngAds Android]:https://bitbucket.org/mngcorp/mngads-demo-android
[Cordova CLI]:https://cordova.apache.org/docs/en/edge/guide_cli_index.md.html#The%20Command-Line%20Interface
[mng-ads-sdk-cordova-plugin]:https://bitbucket.org/mngcorp/mngads-cordova-plugin/src/HEAD/mng-ads-sdk-cordova-plugin/?at=master
[www]:https://bitbucket.org/mngcorp/mngads-private-cordova-plugin/src/HEAD/www/?at=master
[plugin.xml]:https://bitbucket.org/mngcorp/mngads-cordova-plugin/src/HEAD/mng-ads-sdk-cordova-plugin/plugin.xml?at=master&fileviewer=file-view-default
