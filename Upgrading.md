# Upgrading
## v2.3.1

 * createInterstitial() have a new parameter *autoDisplay* that allow you to prepare an interstitial without display it by setting autoDisplay false, but if you dont want to change it, just set it true.


```javascript
 
 this.mngAds.createInterstitial(placementId,preferences,this.adSuccess,this.adError,true);
```