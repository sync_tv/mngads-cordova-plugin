//
//  MAdvertiseBeacon.h
//  MngAds
//
//  Created by MacBook Pro on 11/10/2016.
//  Copyright © 2016 Bensalah Med Amine. All rights reserved.
//

#import "MAdvertiseBeaconAdapter.h"

@interface MAdvertiseBeacon : MAdvertiseBeaconAdapter

+(MAdvertiseBeacon*)singleton;
-(void)initBeacon;

@end
