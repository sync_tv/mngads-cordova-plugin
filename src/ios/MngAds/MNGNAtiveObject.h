//
//  MNGNAtiveObject.h
//  MNG-Ads-SDK
//
//  Created by Ben Salah Med Amine on 12/9/14.
//  Copyright (c) 2014 MNG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MNGPriceType) {
    MNGPriceTypeFree,
    MNGPriceTypePayable,
    MNGPriceTypeUnknown
};

typedef NS_ENUM(NSInteger, MNGDisplayType) {
    MNGDisplayTypeUnknown = 0,
    MNGDisplayTypeAppInstall,
    MNGDisplayTypeContent
};

typedef NS_ENUM(NSInteger, MAdvertiseAssetType) {
    MAdvertiseAssetTypeAppIcon = 0,
    MAdvertiseAssetTypeScrennShot
};

@interface MNGNAtiveObject : NSObject

@property NSString *title;
@property NSString *socialContext;
@property NSString *body;
@property NSString *callToAction;
@property NSURL *photoUrl;
@property NSURL *coverImageUrl;
@property UIView *badgeView;
@property UIView *adChoiceBadgeView;
@property MNGPriceType priceType DEPRECATED_MSG_ATTRIBUTE("Deprecated in MNGAds 2.2");
@property NSString *localizedPrice;
@property MNGDisplayType displayType;
@property (nonatomic, copy) void (^mediaContainerBlock)(UIView *mediaContainer);
@property (nonatomic, copy) void (^downloadAssetBlock)(MAdvertiseAssetType type,void(^completition)(UIImage*));

- (void)registerViewForInteraction:(UIView *)view
                withViewController:(UIViewController *)viewController
                withClickableView:(UIView *)clickableView;

- (void)setMediaContainer:(UIView *)mediaContainer;

- (void)downloadAssetWithType:(MAdvertiseAssetType)type completition:(void(^)(UIImage* image))completition;

@end
