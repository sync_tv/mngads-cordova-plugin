//
//  MNGNativeAd.h
//  MNGAdServerSdk
//
//  Copyright (c) 2015 MNG. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MNGNativeAdDelegate;

typedef NS_ENUM(NSUInteger, MNGScreenshotOrientation) {
   MNGScreenshotOrientationUnknown = 0,
   MNGScreenshotOrientationPortrait,
   MNGScreenshotOrientationLandscape
};

typedef NS_ENUM(NSUInteger, MNGScreenshotType) {
   MNGScreenshotTypeUnknown = 0,
   MNGScreenshotTypeiPhone,
   MNGScreenshotTypeiPad
};

typedef NS_ENUM(NSUInteger, MNGNativeAdType) {
   MNGNativeAdTypeSushi = 1,
   MNGNativeAdTypeHimono = 2,
   MNGNativeAdTypeNative = 3,
   MNGNativeAdTypeSashimi = 5
};

typedef NS_ENUM(NSInteger, MAdvertiseServerAssetType) {
    MAdvertiseServerAssetTypeAppIcon = 0,
    MAdvertiseServerAssetTypeScrennShot
};

@interface MNGNativeAd : NSObject

-(void)setCacheDisplayableAssets:(BOOL)doCache;
- (void)downloadAssetWithType:(MAdvertiseServerAssetType)type completition:(void(^)(UIImage* image))completition;
-(void)loadAd;
-(void)registerViewForInteraction:(UIView *)view withClickableView:(UIView *)clickableView;
-(void)connectViewForDisplay:(UIView *)view withClickableViews:(NSArray *)clickableViews;
-(void)disconnectViewForDisplay;
-(void)setMediaContainer:(UIView*)mediaContainer;
-(UIView *)getBadgeView;

@property NSString *publisherId;
@property (weak, nonatomic) id<MNGNativeAdDelegate> delegate;
@property (assign) MNGNativeAdType nativeAdType;
@property (assign) NSString *keyWords;
@property BOOL acceptVideo;

@property (readonly, copy) NSNumber *adid;
@property (readonly, copy) NSNumber *autoclose;
@property (readonly, copy) NSNumber *averageUserRating;
@property (readonly, copy) NSNumber *bundleID;
@property (readonly, copy) NSString *callToActionTitle;
@property (readonly, copy) NSString *localizedCallToActionTitle;
@property (readonly, copy) NSString *category;
@property (readonly, copy) NSString *categoryID;
@property (readonly, copy) NSString *clickType;
@property (readonly, copy) NSString *clickURL;
@property (readonly, copy) NSNumber *closeAppearanceDelay;
@property (readonly, copy) NSString *closePosition;
@property (readonly, copy) NSString *contentRating;
@property (readonly, copy) NSString *tagline;
@property (readonly, copy) NSString *iconURL;
@property (readonly, copy) NSArray *impURL;
@property (readonly, copy) NSNumber *price;
@property (readonly, copy) NSString *localizedPrice;
@property (readonly, copy) NSNumber *refresh;
@property (readonly, copy) NSArray *screenshotsURLs;
@property (readonly, copy) NSString *title;
@property (readonly) BOOL isVideo;
@property (readonly, copy) NSNumber *userRatingCount;
@property (nonatomic, strong) NSArray *iconAvgColorComponents;
@property (readonly, assign) MNGScreenshotOrientation screenshotsOrientation;
@property (readonly, assign) MNGScreenshotType screenshotsType;
@property (readonly, assign) BOOL isFree DEPRECATED_MSG_ATTRIBUTE("Deprecated in MNGAds 2.2");

@end

@protocol MNGNativeAdDelegate <NSObject>

@optional

-(void)nativeAdDidLoad:(MNGNativeAd *)nativeAd;
-(void)nativeAd:(MNGNativeAd *)nativeAd didFailWithError:(NSError *)error;
-(void)nativeAdWasClicked:(MNGNativeAd *)nativeAd;

@end
