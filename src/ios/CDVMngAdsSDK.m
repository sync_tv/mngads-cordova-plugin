/*!
 *  @header    CDVMngAdsSDK.m
 *  @abstract  Cordova Plugin for the Mng Ads iOS SDK.
 *  @version   1.0.10
 */

#import "CDVMngAdsSDK.h"
#import <Cordova/CDVViewController.h>
#import "MNGPreference.h"
#import "MNGNAtiveObject.h"
#import "MAdvertiseBeacon.h"

@implementation CDVMngAdsSDK{
    MNGAdsSDKFactory *interstitialAdsFactory;
    MNGAdsSDKFactory *bannerAdsFactory;
    MNGAdsSDKFactory *nativeAdsFactory;
    NSString *interstitialCallbackId;
    NSString *bannerCallbackId;
    NSString *nativeCallbackId;
    NSString *sdkCallbackId;
    UIView *banner;
    UIWebView *webView;
    UIView *nativeView;
    BOOL bannerShowen;
    int height;
    NSString *position;
    BOOL autoDisplay;
    UIView *mediaContainer;
    MNGNAtiveObject *_nativeObject;
}

- (void)pluginInitialize {
    [super pluginInitialize];
    bannerShowen = NO;
    webView = [(CDVViewController*)[self viewController] webView];
    webView.scrollView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - MNG Ads SDK

-(MNGPreference *)preferencesWithString:( NSObject *)pref{
    if (!pref || [pref isEqual:@""]) {
        return nil;
    }
    NSDictionary *dict;
    if ([pref isKindOfClass:[NSString class]]) {
        NSString *jsonString = pref;
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        dict = [NSJSONSerialization JSONObjectWithData:data
                                               options:0
                                                 error:NULL];
    }else if([pref isKindOfClass:[NSDictionary class]]){
        dict = pref;
    }else{
        return nil;
    }
    //    "preferences": {
    //        "age": "25",
    //        "language": "fr",
    //        "keyword": "brand=myBrand;category=sport",
    //        "gender": "M",
    //        "location": {
    //            "lat": "48.876",
    //            "lon": "10.453"
    //        }
    //    },
    if (!dict) {
        return nil;
    }
    MNGPreference *preferences = [[MNGPreference alloc]init];
    if (dict[@"age"]) {
        preferences.age = [dict[@"age"]integerValue];
    }
    if (dict[@"language"]) {
        preferences.language = dict[@"language"];
    }
    if (dict[@"keyword"]) {
        preferences.keyWord = dict[@"keyword"];
    }
    if (dict[@"gender"]) {
        if ([dict[@"gender"] isEqualToString:@"M"]) {
            preferences.gender = MNGGenderMale;
        }else if ([dict[@"gender"] isEqualToString:@"F"]) {
            preferences.gender = MNGGenderFemale;
        }else{
            preferences.gender = MNGGenderUnknown;
        }
    }
    
    if (dict[@"location"]) {
        if (dict[@"location"][@"lat"] && dict[@"location"][@"lon"]) {
            preferences.location = [[CLLocation alloc]initWithLatitude:[dict[@"location"][@"lat"]floatValue]
                                                             longitude:[dict[@"location"][@"lon"]floatValue]];
        }
    }
    
    return preferences;
}

- (void)mngadssdk_initWithAppId:(CDVInvokedUrlCommand *)command{
    NSArray *arguments = command.arguments;
    NSString *appId = [arguments objectAtIndex:0];
    sdkCallbackId = command.callbackId;
    dispatch_async(dispatch_get_main_queue(), ^{
        [MNGAdsSDKFactory initWithAppId:appId];
        if ([MNGAdsSDKFactory isInitialized]) {
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:sdkCallbackId];
        }else{
            [MNGAdsSDKFactory setDelegate:self];
        }
    });
}

-(void)mngadssdk_initBeacons:(CDVInvokedUrlCommand *)command{
    [[MAdvertiseBeacon singleton]initBeacon];
}

-(void)MNGAdsSDKFactoryDidFinishInitializing{
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:sdkCallbackId];
}


- (void)mngadssdk_isInitialized:(CDVInvokedUrlCommand *)command{
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:[MNGAdsSDKFactory isInitialized]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


- (void)mngadssdk_createInterstitial:(CDVInvokedUrlCommand *)command{
    NSArray *arguments = command.arguments;
    interstitialCallbackId = command.callbackId;
    NSString *placementId = [arguments objectAtIndex:0];
    NSString *pref = [arguments objectAtIndex:1];
    BOOL _autoDisplay = [[arguments objectAtIndex:2]boolValue];
    if(interstitialAdsFactory == nil)interstitialAdsFactory = [[MNGAdsSDKFactory alloc]init];
    interstitialAdsFactory.interstitialDelegate = self;
    interstitialAdsFactory.viewController = [self viewController];
    interstitialAdsFactory.placementId = placementId;
    dispatch_async(dispatch_get_main_queue(), ^{
        bool ok = [interstitialAdsFactory createInterstitialWithPreferences:[self preferencesWithString:pref]autoDisplayed:_autoDisplay];
        if (!ok) {
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail to create"];
            [pluginResult setKeepCallbackAsBool:YES];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:interstitialCallbackId];
        }
    });
}

- (void)mngadssdk_showInterstitial:(CDVInvokedUrlCommand *)command{
    if ([interstitialAdsFactory isInterstitialReady]) {
        [interstitialAdsFactory displayInterstitial];
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"DID_BE_SHOWEN"];
        [pluginResult setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }else{
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail to display interstitial"];
        [pluginResult setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}


- (void)mngadssdk_debugEnable:(CDVInvokedUrlCommand *)command{
    NSArray *arguments = command.arguments;
    BOOL enabled = [[arguments objectAtIndex:0]boolValue];
    [MNGAdsSDKFactory setDebugModeEnabled:enabled];
}


- (void)mngadssdk_createBanner:(CDVInvokedUrlCommand *)command{
    NSArray *arguments = command.arguments;
    bannerCallbackId = command.callbackId;
    //[placementId,height,position,autoDisplay]
    NSString *placementId = [arguments objectAtIndex:0];
    int mheight = [[arguments objectAtIndex:1]intValue];
    NSString *pref = [arguments objectAtIndex:4];
    if(bannerAdsFactory == nil)bannerAdsFactory = [[MNGAdsSDKFactory alloc]init];
    bannerAdsFactory.bannerDelegate = self;
    bannerAdsFactory.viewController = [self viewController];
    bannerAdsFactory.placementId = placementId;
    CGRect frame;
    CGFloat screenWidth = [self screenWidth];
    if (height>=250) {
        frame = CGRectMake(0, 0, 300, height);
    }else{
        frame = CGRectMake(0, 0, screenWidth, height);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if (bannerShowen) {
            [banner removeFromSuperview];
            banner = nil;
            bannerShowen = NO;
            webView.frame = self.viewController.view.bounds;
        }
        bool ok = [bannerAdsFactory createBannerInFrame:frame withPreferences:[self preferencesWithString:pref]];
        if (!ok) {
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail to create"];
            [pluginResult setKeepCallbackAsBool:YES];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:bannerCallbackId];
        }else{
            height = mheight;
            position = [arguments objectAtIndex:2];
            autoDisplay = [[arguments objectAtIndex:3]boolValue];
        }
    });
    
}

- (void)mngadssdk_showBanner:(CDVInvokedUrlCommand *)command{
    if (banner&&!bannerShowen) {
        [self showBanner];
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }else{
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

-(void)showBanner{
    if (bannerShowen) {
        return;
    }
    if ([position isEqualToString:@"TOP"]) {
        [self showBannerFixTop];
    }else{
        [self showBannerFixBottom];
    }
    bannerShowen = YES;
}

-(void)showBannerFixTop{
    CGRect frame;
    CGFloat screenWidth = [self screenWidth];
    if (height>=250) {
        frame = CGRectMake((screenWidth - 300)/2, 20, 300, height);
    }else{
        frame = CGRectMake(0, 20, screenWidth, height);
    }
    banner.frame = frame;
    [self.viewController.view addSubview:banner];
    CGRect webFrame = webView.frame;
    webFrame.origin.y = frame.size.height + 20;
    webFrame.size.height -= frame.size.height + 20;
    webView.frame = webFrame;
}

-(void)showBannerFixBottom{
    CGRect frame;
    CGFloat screenWidth = [self screenWidth];
    CGFloat y = [self screenHeight] - height;
    if (height>=250) {
        frame = CGRectMake((screenWidth - 300)/2, y, 300, height);
    }else{
        frame = CGRectMake(0, y, screenWidth, height);
    }
    banner.frame = frame;
    [self.viewController.view addSubview:banner];
    CGRect webFrame = webView.frame;
    webFrame.size.height -= frame.size.height;
    webView.frame = webFrame;
}

-(void)showBannerScrollTop{
    //
}

-(void)showBannerScrollBottom{
    //
}

- (void)mngadssdk_createNative:(CDVInvokedUrlCommand *)command{
    [self removeNativeView];
    nativeCallbackId = command.callbackId;
    NSArray *arguments = command.arguments;
    NSString *placementId = arguments[0];
    NSString *pref = arguments[1];
//    CGFloat x = [arguments[2]floatValue];
//    CGFloat y = [arguments[3]floatValue];
//    CGFloat width = [arguments[4]floatValue];
//    CGFloat height = [arguments[5]floatValue];
//    UIView *v =[[UIView alloc]initWithFrame:CGRectMake(x,y,width,height)];
//    v.backgroundColor = [UIColor redColor];
//    [webView.scrollView addSubview:v];
    if(nativeAdsFactory == nil)nativeAdsFactory = [[MNGAdsSDKFactory alloc]init];
    nativeAdsFactory.nativeDelegate = self;
    nativeAdsFactory.viewController = [self viewController];
    nativeAdsFactory.placementId = placementId;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        bool ok = [nativeAdsFactory createNativeWithPreferences:[self preferencesWithString:pref]];
        if (!ok) {
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Fail to create"];
            [pluginResult setKeepCallbackAsBool:YES];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:nativeCallbackId];
        }
    });
    
}

- (void)mngadssdk_setNativeAdClickArea:(CDVInvokedUrlCommand *)command{
    nativeCallbackId = command.callbackId;
    NSArray *arguments = command.arguments;
    CGFloat x = [arguments[0]floatValue];
    CGFloat y = [arguments[1]floatValue];
    CGFloat width = [arguments[2]floatValue];
    CGFloat height = [arguments[3]floatValue];
    [nativeView removeFromSuperview];
    nativeView =[[UIView alloc]initWithFrame:CGRectMake(x + webView.scrollView.contentOffset.x,y + webView.scrollView.contentOffset.y,width,height)];
    nativeView.backgroundColor = [[UIColor redColor]colorWithAlphaComponent:0];
    [webView.scrollView addSubview:nativeView];
    [_nativeObject registerViewForInteraction:nativeView withViewController:[self viewController] withClickableView:command.callbackId];
    
}

- (void)mngadssdk_setMediaContainerArea:(CDVInvokedUrlCommand *)command{
    NSArray *arguments = command.arguments;
    CGFloat x = [arguments[0]floatValue];
    CGFloat y = [arguments[1]floatValue];
    CGFloat width = [arguments[2]floatValue];
    CGFloat height = [arguments[3]floatValue];
    if (!mediaContainer) {
        mediaContainer = [[UIView alloc]init];
        [_nativeObject setMediaContainer:mediaContainer];
        [webView.scrollView addSubview:mediaContainer];
    }
    mediaContainer.frame = CGRectMake(x + webView.scrollView.contentOffset.x,y + webView.scrollView.contentOffset.y,width,height);
}

- (void)mngadssdk_removeNativeAd:(CDVInvokedUrlCommand *)command{
    [self removeNativeView];
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"REMOVE_NATIVE"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

-(void)removeNativeView{
    [nativeView removeFromSuperview];
    nativeView = nil;
    _nativeObject = nil;
    [mediaContainer removeFromSuperview];
    mediaContainer = nil;
}

#pragma mark - MNGAdsAdapterNativeDelegate

-(NSString*)jsonFromNativeObject:(MNGNAtiveObject *)nativeObject{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if (nativeObject.title) {
        dict[@"title"] = nativeObject.title;
    }
    if (nativeObject.body) {
        dict[@"body"] = nativeObject.body;
    }
    if (nativeObject.socialContext) {
        dict[@"social_context"] = nativeObject.socialContext;
    }
    if (nativeObject.callToAction) {
        dict[@"call_to_action"] = nativeObject.callToAction;
    }
    if (nativeObject.photoUrl) {
        dict[@"ad_icon_url"] = nativeObject.photoUrl.description;
    }
    if (nativeObject.coverImageUrl) {
        dict[@"ad_cover_url"] = nativeObject.coverImageUrl.description;
    }
    if (nativeObject.badgeView) {
        UIGraphicsBeginImageContextWithOptions(nativeObject.badgeView.bounds.size, NO, [UIScreen mainScreen].scale) ;
        [nativeObject.badgeView.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        NSData * data = [UIImagePNGRepresentation(resultingImage) base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
        dict[@"badge_url"] = [NSString stringWithFormat:@"data:image/png;base64,%@",[NSString stringWithUTF8String:[data bytes]]];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

-(void)adsAdapter:(MNGAdsAdapter *)adsAdapter nativeObjectDidLoad:(MNGNAtiveObject *)nativeObject{
    _nativeObject = nativeObject;
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[self jsonFromNativeObject:nativeObject]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:nativeCallbackId];
}

-(void)adsAdapter:(MNGAdsAdapter *)adsAdapter nativeObjectDidFailWithError:(NSError *)error{
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.description];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:nativeCallbackId];
}


#pragma mark - MNGAdsAdapterBannerDelegate

-(void)adsAdapter:(MNGAdsAdapter *)adsAdapter bannerDidLoad:(UIView *)adView preferredHeight:(CGFloat)preferredHeight{
    banner = adView;
    if (autoDisplay) {
        [self showBanner];
    }
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:bannerCallbackId];
}

-(void)adsAdapter:(MNGAdsAdapter *)adsAdapter bannerDidFailWithError:(NSError *)error{
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.description];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:bannerCallbackId];
}

#pragma mark - MNGAdsAdapterInterstitialDelegate

-(void)adsAdapterInterstitialDidLoad:(MNGAdsAdapter *)adsAdapter{
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"DID_LOAD"];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:interstitialCallbackId];
}

-(void)adsAdapterInterstitialDisappear:(MNGAdsAdapter *)adsAdapter{
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"DID_DISAPPEAR"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:interstitialCallbackId];
}

-(void)adsAdapter:(MNGAdsAdapter *)adsAdapter interstitialDidFailWithError:(NSError *)error{
    CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.description];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:interstitialCallbackId];
}

#pragma mark - LifeCycle

-(void)onAppTerminate{
    NSLog(@"onAppTerminate");
    [interstitialAdsFactory releaseMemory];
    interstitialAdsFactory = nil;
    [bannerAdsFactory releaseMemory];
    bannerAdsFactory = nil;
    [super onAppTerminate];
}

-(CGFloat)screenWidth{
    return webView.frame.size.width;
}

-(CGFloat)screenHeight{
    return webView.frame.size.height;
}

@end
