package com.mngads.cordova;
import java.io.ByteArrayOutputStream;
import android.widget.Button;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.RelativeLayout.LayoutParams;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import com.mngads.MNGAdsFactory;
import com.mngads.MNGNativeObject;
import com.mngads.listener.MNGAdsSDKFactoryListener;
import com.mngads.listener.MNGBannerListener;
import com.mngads.listener.MNGInterstitialListener;
import com.mngads.listener.MNGNativeListener;
import com.mngads.util.MNGDebugLog;
import com.mngads.util.MNGFrame;
import com.mngads.util.MNGGender;
import com.mngads.util.MNGPreference;
import com.mngads.util.MNGPriceType;
import com.mngads.util.MNGUtils;
import com.mngads.MAdvertiseBeaconAdapter;

import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;


public class CDVMngAdsSDK extends CordovaPlugin implements
	MNGInterstitialListener, MNGBannerListener,MNGNativeListener, MNGAdsSDKFactoryListener {

    private static final boolean CORDOVA_MIN_4 = Integer
	    .valueOf(CordovaWebView.CORDOVA_VERSION.split("\\.")[0]) >= 4;

    /**
     * Cordova Actions
     */
    private final String ACTION_INITIALIZE_BECONS = "mngadssdk_initBeacons";
    private final String ACTION_INITIALIZE = "mngadssdk_initWithAppId";
    private final String ACTION_CREATE_INTERSTITIAL = "mngadssdk_createInterstitial";
    private final String ACTION_CREATE_BANNER = "mngadssdk_createBanner";
    private final String ACTION_SHOW_BANNER = "mngadssdk_showBanner";
    private final String ACTION_DEBUG_ENABLE = "mngadssdk_debugEnable";
    private final String ACTION_IS_INITIALIZED = "mngadssdk_isInitialized";
    private final String ACTION_CREATE_NATIVE = "mngadssdk_createNative";
    private final String ACTION_UPDATE_NATIVE_CLICK_AREA = "mngadssdk_setNativeAdClickArea";
    private final String ACTION_REMOVE_NATIVE = "mngadssdk_removeNativeAd";
    private final String ACTION_SET_MEDIA_CONTAINER_NATIVE = "mngadssdk_setMediaContainerArea";
    private final String ACTION_SHOW_INTERSTITIAL = "mngadssdk_showInterstitial";
    
	private Window window;
    
    
    /**
     * MNGAds Factory to create banner/interstitial Ad
     */
    private MNGAdsFactory mMNGAdsInterstitialAdsFactory;
    private MNGAdsFactory mMNGAdsBannerAdsFactory;
    private MNGAdsFactory mMNGAdsNativeAdsFactory;

    /**
     * MNGAds Banner view
     */
    private View mMNGAdView;
    /**
     * MNGAds Banner visibility
     */
    private boolean mIsMNGAdVisible;
    private boolean mAutoDisplay;
    private boolean mTopAd;
    private boolean mIsDebug;

    private final String TOP = "TOP";
    /**
     * MNGpreference keys
     */
    public final String PREFERENCE_AGE = "age";
    public final String PREFERENCE_LANGUAGE = "language";
    public final String PREFERENCE_KEYWORD = "keyword";
    public final String PREFERENCE_GENDER = "gender";
    public final String PREFERENCE_GENDER_MALE = "M";
    public final String PREFERENCE_GENDER_FEMALE = "F";
    public final String PREFERENCE_GENDER_UNKONOWN = "U";
    public final String PREFERENCE_LOCATION = "location";
    public final String PREFERENCE_LAT = "lat";
    public final String PREFERENCE_LON = "lon";

    /**
     * excute create interstitial callBack
     */
    private CallbackContext mInterstitialCallBack;
    /**
     * excute create Banner callBack
     */
    private CallbackContext mBannerCallBack;

    /**
     * excute mng initialization callBack
     */
    private CallbackContext mInitializationCallBack;
     /**
     * excute create Native callBack
     */
    private CallbackContext mNativeCallBack;


    private ViewGroup mParentView;
    private FrameLayout mNativeContainerLayer;
    /**
     * native click layer
     */
    private Button mNativeClickLayer;
    
    /**
     * media view
     */
    private     FrameLayout mMediaView ;
     /**
     * MNG Native Ad keys
     */

    private final String NATIVE_TITLE = "title";
    private final String NATIVE_BODY = "body";
    private final String NATIVE_SOCIAL_CONTEXT = "social_context";
    private final String NATIVE_CALL_TO_ACTION = "call_to_action";
    private final String NATIVE_AD_ICON = "ad_icon_url";
    private final String NATIVE_AD_COVER_IMAGE = "ad_cover_url";
    private final String NATIVE_PRICE_TEXT = "price_text";
    private final String NATIVE_PRICE_TYPE = "price_type";
    private final String NATIVE_BADGE_URL = "badge_url";
    // price type
    private final String MNG_PRICE_TYPE_FREE = "free";
    private final String MNG_PRICE_TYPE_PAYABLE = "payable";
    private final String MNG_PRICE_TYPE_UNKNOWN = "unknown";

    // mng Native object for native Ad
    private MNGNativeObject mMNGNativeObject;
    
      private final static String TAG = CDVMngAdsSDK.class.getSimpleName();
    // Initialize
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
	super.initialize(cordova, webView);
	window = cordova.getActivity().getWindow();
	window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
	
    }
   
   

    /**
     * This is the main method for the MNGAds plugin. All API calls go through
     * here. This method determines the action, and executes the appropriate
     * call.
     *
     * @param action
     *            The action that the plugin should execute.
     * @param options
     *            The input parameters for the action.
     * @param callbackContext
     *            The callback context.
     * @return returned if the action is recognized.
     */
    @Override
    public boolean execute(String action, JSONArray options,
	    CallbackContext callbackContext) throws JSONException {

	try {

		if(action.equals(ACTION_INITIALIZE_BECONS))
		{
          MAdvertiseBeaconAdapter.initBeacons(cordova.getActivity().getApplicationContext());	
		}
	    else if (action.equals(ACTION_INITIALIZE)) {

		MNGAdsFactory.initialize(cordova.getActivity(),
			options.optString(0));

		mInitializationCallBack = callbackContext;

		if (MNGAdsFactory.isInitialized()) {

		    mInitializationCallBack.success();

		} else {

		    MNGAdsFactory.setMNGAdsSDKFactoryListener(this);

		}

		return true;

	    } else if (action.equals(ACTION_CREATE_INTERSTITIAL)) {

		executeCreateInterstitialView(options, callbackContext);

		return true;

	    } else if (action.equals(ACTION_CREATE_BANNER)) {

		executeCreateBannerView(options, callbackContext);

		return true;

	    } else if (action.equals(ACTION_SHOW_BANNER)) {

		executeShowBanner(callbackContext);

		return true;

	    } else if (action.equals(ACTION_DEBUG_ENABLE)) {

		MNGAdsFactory.setDebugModeEnabled(options.optBoolean(0));

		return true;

	    } else if (action.equals(ACTION_IS_INITIALIZED)) {

		callbackContext
			.success((MNGAdsFactory.isInitialized()) ? 1 : 0);

		return true;
	    }else if (action.equals(ACTION_CREATE_NATIVE)) {

		executeCreateNative(options, callbackContext);

		return true;

	    } else if (action.equals(ACTION_UPDATE_NATIVE_CLICK_AREA)) {

		if (mMNGNativeObject == null) {

		    callbackContext
			    .error("You must call create native Ad first");

		} else {

		    updateNativeAdClickArea(options);

		    callbackContext.success();
		}
		return true;

	    } else if (action.equals(ACTION_REMOVE_NATIVE)) {

		if (mMNGNativeObject == null) {

		    callbackContext
			    .error("You must call create native Ad first");

		}
		 else {

		    excuteRemoveNative();

		    callbackContext.success("REMOVE_NATIVE");
		}

		return true;
	    }
	    else if (action.equals(ACTION_SET_MEDIA_CONTAINER_NATIVE))
		{

		setMediaContainer(options);	
		return true;	
		}
		else if(action.equals(ACTION_SHOW_INTERSTITIAL))
		{
				executeShowInterstitialView(callbackContext);
						return true;	

		}

	} catch (Exception e) {

	    MNGDebugLog.e(TAG, "Exception in execute" + e.toString());
	}

	return false;
    }




    @Override
    public void onDestroy() {

	cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {

		if (mMNGAdsInterstitialAdsFactory != null) {
		    /** relasing mng ads factory */
		    mMNGAdsInterstitialAdsFactory.releaseMemory();
		}

		if (mMNGAdsBannerAdsFactory != null) {
		    /** relasing mng ads factory */
		    mMNGAdsBannerAdsFactory.releaseMemory();
		}

	    }
	});

	super.onDestroy();
    }
 /**
     * Run to show MNGInterstitial
     *
      * @param callbackContext
     *            The callback context.
     */

private void executeShowInterstitialView(CallbackContext callbackContext) {
// get callBack to be called on interstitial show/fail
mInterstitialCallBack = callbackContext;

cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {	
		if(mMNGAdsInterstitialAdsFactory.isInterstitialReady())
		{
			mMNGAdsInterstitialAdsFactory.displayInterstitial();
			PluginResult pluginResult = new PluginResult(
			    PluginResult.Status.OK, "Interstitial displayed");


		    mInterstitialCallBack.sendPluginResult(pluginResult);
		}
		else {
			
			PluginResult pluginResult = new PluginResult(
			    PluginResult.Status.ERROR, "Failed to display");


		    mInterstitialCallBack.sendPluginResult(pluginResult);
		}
		 }
});
    }

    /**
     * Run to create MNGInterstitial
     *
     * @param options
     *            The JSONArray representing input parameters.
     * @param callbackContext
     *            The callback context.
     */
    private void executeCreateInterstitialView(final JSONArray options,
	    CallbackContext callbackContext) {

	// get callBack to be called on interstitial load/fail/disappear
	mInterstitialCallBack = callbackContext;
	// get placement id
	final String placementId = options.optString(0);
	// get user preference
	final MNGPreference preference = getPreference(options.optString(1));
	final boolean mAutoDisplay=options.optBoolean(2);
	// excute create MNG Ads interstitial on the UI Thread
	cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		// instantiate mng Ads Factory
		if (mMNGAdsInterstitialAdsFactory == null) {
		    mMNGAdsInterstitialAdsFactory = new MNGAdsFactory(cordova
			    .getActivity());
		}

		// set MNG placement Id
		mMNGAdsInterstitialAdsFactory.setPlacementId(placementId);
		// set MNG listener
		mMNGAdsInterstitialAdsFactory
			.setInterstitialListener(CDVMngAdsSDK.this);
		// create interstitial with preference
		if (!mMNGAdsInterstitialAdsFactory
			.createInterstitial(preference,mAutoDisplay)) {
		    // mMNGAdsInterstitialAdsFactory is not initialized or is
		    // busy
		    PluginResult pluginResult = new PluginResult(
			    PluginResult.Status.ERROR, "Failed to create");

		    pluginResult.setKeepCallback(true);

		    mInterstitialCallBack.sendPluginResult(pluginResult);

		}

	    }
	});
    }

    /**
     * Run to create MNGBanner
     *
     * @param options
     *            The JSONArray representing input parameters.
     * @param callbackContext
     *            The callback context.
     */
    private void executeCreateBannerView(final JSONArray options,
	    final CallbackContext callbackContext) {

	// get callBack to be called on banner load/fail
	mBannerCallBack = callbackContext;
	// get placement id
	final String placementId = options.optString(0);
	// get device width
	final int widthPx = getScreenWidth(cordova.getActivity());
	// get requested ad height
	final int heightDp = options.optInt(1);
	// convert device width from pix to dp
	final int widthDp = (int) MNGUtils.convertPixelsToDp(widthPx,
		cordova.getActivity());
	// get user preference
	final MNGPreference preference = getPreference(options.optString(4));
	// excute create MNG Ads banner on the UI Thread
	cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		// remove MNG Ad view from screen
		if (mMNGAdView != null) {

		    ViewGroup adParent = (ViewGroup) (mMNGAdView.getParent());
		    if (adParent != null) {
			adParent.removeView(mMNGAdView);
		    }

		    mMNGAdView = null;
		    mIsMNGAdVisible = false;

		}

		// instantiate mng Ads Factory
		if (mMNGAdsBannerAdsFactory == null) {
		    mMNGAdsBannerAdsFactory = new MNGAdsFactory(cordova
			    .getActivity());
		}
		// set MNG placement Id
		mMNGAdsBannerAdsFactory.setPlacementId(placementId);
		// set MNG listener
		mMNGAdsBannerAdsFactory.setBannerListener(CDVMngAdsSDK.this);
		// create interstitial with preference
		if (!mMNGAdsBannerAdsFactory.createBanner(new MNGFrame(widthDp,
			heightDp), preference)) {

		    // mMNGAdsBannerAdsFactory is not initialized or is busy
		    PluginResult pluginResult = new PluginResult(
			    PluginResult.Status.ERROR, "Failed to create");

		    pluginResult.setKeepCallback(true);

		    mBannerCallBack.sendPluginResult(pluginResult);

		} else {
		    // get Ad position
		    String position = options.optString(2);
		    if (position.equals(TOP)) {
			mTopAd = true;
		    } else {
			mTopAd = false;
		    }
		    // auto display Ad or not
		    mAutoDisplay = options.optBoolean(3);
		}

	    }
	});

    }

    /**
     * 
     * Run to show Banner
     * 
     * @param callbackContext
     *            The callback context.
     */
    private void executeShowBanner(CallbackContext callbackContext) {

	// check that MNG Ad view not null and not visible
	if (mMNGAdView != null && !mIsMNGAdVisible) {

	    // display MNG Banner on screen
	    showBanner();

	    // call success
	    callbackContext.success();

	} else {
	    // call failed to display banner on screen
	    callbackContext.error("Failed to show");
	}

    }
     /**
     * Run to create MNGNative Ad
     *
     * @param options
     *            The JSONArray representing input parameters.
     * @param callbackContext
     *            The callback context.
     */
    private void executeCreateNative(final JSONArray options,
	    final CallbackContext callbackContext) {

	// get callBack to be called on native load/fail
	mNativeCallBack = callbackContext;
	// get placement id
	final String placementId = options.optString(0);
	// get user preference
	final MNGPreference preference = getPreference(options.optString(1));
	// excute create MNG native Ad on the UI Thread
	cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		// instantiate mng Ads Factory
		if (mMNGAdsNativeAdsFactory == null) {
		    mMNGAdsNativeAdsFactory = new MNGAdsFactory(cordova
			    .getActivity());
		}

		if (mMNGNativeObject != null) {
		    initializeNative();
		}

		// set MNG placement Id
		mMNGAdsNativeAdsFactory.setPlacementId(placementId);
		// set MNG listener
		mMNGAdsNativeAdsFactory.setNativeListener(CDVMngAdsSDK.this);
		// create interstitial with preference
		if (!mMNGAdsNativeAdsFactory.createNative(preference)) {
		    // mMNGAdsInterstitialAdsFactory is not initialized or is
		    // busy
		    PluginResult pluginResult = new PluginResult(
			    PluginResult.Status.ERROR, "Failed to create");

		    pluginResult.setKeepCallback(true);

		    mNativeCallBack.sendPluginResult(pluginResult);

		}

	    }
	});
    }
      /**
     * update native Ad click area container depanding on cordova web view
     */
    private void updateNativeContainer() {

	FrameLayout.LayoutParams containerParams = (FrameLayout.LayoutParams) mNativeContainerLayer
		.getLayoutParams();

	containerParams.width = getWebView().getWidth();

	containerParams.height = getWebView().getHeight();

	containerParams.setMargins(getWebView().getLeft(), getWebView()
		.getTop(), 0, 0);

	mNativeContainerLayer.setLayoutParams(containerParams);
    }

    /**
     * Run remove native Ad click area
     */
    private void excuteRemoveNative() {
	cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {

		initializeNative();
	    }
	});
    }

   public void setMediaContainer(final JSONArray options)
    {
    	cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		int x = (int) MNGUtils.convertDpToPixel(options.optInt(0),
			cordova.getActivity());
		int y = (int) MNGUtils.convertDpToPixel(options.optInt(1),
			cordova.getActivity());
		int w = (int) MNGUtils.convertDpToPixel(options.optInt(2),
			cordova.getActivity());
		int h = (int) MNGUtils.convertDpToPixel(options.optInt(3),
			cordova.getActivity());

            FrameLayout.LayoutParams containerParams;
			if(mMediaView==null)
			{
            mMediaView = new FrameLayout(cordova.getActivity());
            containerParams = new FrameLayout.LayoutParams(
			    w, h);

		    containerParams.setMargins(x,y, 0, 0);
		    		    containerParams.gravity = Gravity.TOP;

		    mMNGNativeObject.setMediaContainer(mMediaView);	
		    cordova.getActivity().addContentView(mMediaView,containerParams);

			}
			else {

				containerParams=(FrameLayout.LayoutParams)mMediaView.getLayoutParams();
			    containerParams.setMargins(x,y, 0, 0);
		    	mMediaView.setLayoutParams(containerParams);
			}
			
		    
		    
	
      
     }
	});
    }

     /**
     * update native click Area position
     */
    public void updateNativeAdClickArea(final JSONArray options) {

	cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {

		int x = (int) MNGUtils.convertDpToPixel(options.optInt(0),
			cordova.getActivity());
		int y = (int) MNGUtils.convertDpToPixel(options.optInt(1),
			cordova.getActivity());
		int w = (int) MNGUtils.convertDpToPixel(options.optInt(2),
			cordova.getActivity());
		int h = (int) MNGUtils.convertDpToPixel(options.optInt(3),
			cordova.getActivity());

		if (mNativeContainerLayer == null) {

		    mNativeContainerLayer = new FrameLayout(cordova
			    .getActivity());

		    FrameLayout.LayoutParams containerParams = new FrameLayout.LayoutParams(
			    getWebView().getWidth(), getWebView().getHeight());

		    containerParams.setMargins(getWebView().getLeft(),
			    getWebView().getTop(), 0, 0);

		    containerParams.gravity = Gravity.TOP;

		    cordova.getActivity().addContentView(mNativeContainerLayer,
			    containerParams);

		    if (mIsDebug) {
			mNativeContainerLayer.setBackgroundColor(Color
				.parseColor("#5081DAF5"));
		    }

		} else {

		    updateNativeContainer();

		}

		FrameLayout.LayoutParams clickParams = new FrameLayout.LayoutParams(
			w, h);

		clickParams.setMargins(x, y, 0, 0);

		if (mNativeClickLayer == null) {

		    mNativeClickLayer = new Button(cordova.getActivity());
		    mNativeClickLayer.setBackgroundColor(Color.TRANSPARENT);	
		    mNativeContainerLayer.addView(mNativeClickLayer,
			    clickParams);

		    mMNGNativeObject.registerViewForInteraction(mNativeClickLayer);

		    if (mIsDebug) {
			mNativeClickLayer.setBackgroundColor(Color
				.parseColor("#80045FB4"));
		    }

		} else {

		    if (mNativeClickLayer.getParent() == mNativeContainerLayer) {

			mNativeClickLayer.setLayoutParams(clickParams);

		    } else {

			((ViewGroup) mNativeClickLayer.getParent())
				.setLayoutParams(clickParams);

		    }

		}

	    }
	});
    }

    /**
     * initialize native Ad
     */
    private void initializeNative() {
	if(mNativeContainerLayer==null)
	    return ;
	ViewGroup parent = (ViewGroup) mNativeContainerLayer.getParent();
	if(parent!=null)
	{
	parent.removeView(mNativeContainerLayer);
	}
	mNativeContainerLayer = null;
	mNativeClickLayer = null;
	mMNGNativeObject = null;
	ViewGroup mMediaparent = (ViewGroup) mMediaView.getParent();
	if(mMediaparent!=null)
	{
		mMediaparent.removeView(mMediaView);
	}
	mMediaView=null;

    }

    /**
     * Display MNG Banner on screen
     */
    private void showBanner() {
	// show MNG Ads banner
	cordova.getActivity().runOnUiThread(new Runnable() {
	    @Override
	    public void run() {

		// get Cordova web view parent
		ViewGroup wvParentView = (ViewGroup) getWebView().getParent();
		if (wvParentView != null) {
		    wvParentView.removeView(getWebView());
		}

		// create new content view
		mParentView = new LinearLayout(webView.getContext());
		((LinearLayout) mParentView)
			.setOrientation(LinearLayout.VERTICAL);
		((LinearLayout) mParentView).setGravity(Gravity.CENTER);
		mParentView.setLayoutParams(new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.MATCH_PARENT,
			LinearLayout.LayoutParams.MATCH_PARENT));

		// add web view to the new content view
		getWebView().setLayoutParams(
			new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT, 1.0F));
		mParentView.addView(getWebView());

		// add MNG Ads Banner to the screen
		if (mTopAd) {
		    mParentView.addView(mMNGAdView, 0);
		} else {
		    mParentView.addView(mMNGAdView);
		}

		// display the new content on screen
		cordova.getActivity().setContentView(mParentView);
		mIsMNGAdVisible = true;

	    }
	});
    }

    /**
     * get cordova webview
     * 
     */
    private View getWebView() {

	if (CORDOVA_MIN_4) {
	    try {

		return (View) webView.getClass().getMethod("getView")
			.invoke(webView);
	    } catch (Exception e) {
		return (View) webView;
	    }

	} else {

	    return (View) webView;

	}
    }

    /**
     * MNG Preference from String
     */
    private MNGPreference getPreference(String preference) {

	if (preference.isEmpty())
	    return null;

	MNGPreference mMNGPreferance = new MNGPreference();

	try {

	    JSONObject mPreference = new JSONObject(preference);

	    // get user age
	    if (mPreference.has(PREFERENCE_AGE)) {
		mMNGPreferance.setAge(mPreference.getInt(PREFERENCE_AGE));
	    }

	    // get keyword
	    if (mPreference.has(PREFERENCE_KEYWORD)) {
		mMNGPreferance.setKeyword(mPreference
			.getString(PREFERENCE_KEYWORD));
	    }

	    // get user language
	    if (mPreference.has(PREFERENCE_LANGUAGE)) {
		mMNGPreferance.setLanguage(mPreference
			.getString(PREFERENCE_LANGUAGE));
	    }

	    // get user location
	    if (mPreference.has(PREFERENCE_LOCATION)) {
		Location location = new Location("CDV");
		JSONObject jsonLocation = mPreference
			.getJSONObject(PREFERENCE_LOCATION);

		location.setLatitude(jsonLocation.getDouble(PREFERENCE_LAT));
		location.setLongitude(jsonLocation.getDouble(PREFERENCE_LON));
		mMNGPreferance.setLocation(location);

	    }

	    // get user gender
	    if (mPreference.has(PREFERENCE_GENDER)) {
		String gender = mPreference.getString(PREFERENCE_GENDER);
		if (gender.equals(PREFERENCE_GENDER_FEMALE)) {
		    mMNGPreferance.setGender(MNGGender.MNGGenderFemale);
		} else if (gender.equals(PREFERENCE_GENDER_MALE)) {
		    mMNGPreferance.setGender(MNGGender.MNGGenderMale);
		} else {
		    mMNGPreferance.setGender(MNGGender.MNGGenderUnknown);
		}

	    }

	} catch (JSONException ex) {

	    MNGDebugLog.e(TAG, ex.toString());

	    return null;

	}

	return mMNGPreferance;

    }

    /**
     * Convert MNGNative object to string
     */
    private String getString(MNGNativeObject nativeObject) {
	try {
	    JSONObject jsonData = new JSONObject();
	    // put native Ad title
	    jsonData.put(NATIVE_TITLE, nativeObject.getTitle());
	    // put native Ad body
	    jsonData.put(NATIVE_BODY, nativeObject.getBody());
	    // put native social context
	    jsonData.put(NATIVE_SOCIAL_CONTEXT, nativeObject.getSocialContext());
	    // put native call t action
	    jsonData.put(NATIVE_CALL_TO_ACTION, nativeObject.getCallToAction());
	    // put native icon url
	    jsonData.put(NATIVE_AD_ICON, nativeObject.getAdIconUrl());
	    // put native cover url
	    jsonData.put(NATIVE_AD_COVER_IMAGE,
		    nativeObject.getAdCoverImageUrl());
	  
	    // put badge
	    jsonData.put(NATIVE_BADGE_URL,getNativeBadge(nativeObject.getBadge()));

      
	    return jsonData.toString();

	} catch (JSONException ex) {

	    MNGDebugLog.e(TAG, ex.toString());

	    return null;
	}
    }
     /**
     * create string image from native badge
     */
    private String getNativeBadge(Bitmap badge) {
	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	badge.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
	byte[] byteArray = byteArrayOutputStream.toByteArray();
	return "data:image/png;base64,"
		+ Base64.encodeToString(byteArray, Base64.DEFAULT);

    }
    private int getScreenWidth(Context context) {
	WindowManager wm = (WindowManager) context
		.getSystemService(Context.WINDOW_SERVICE);
	Display display = wm.getDefaultDisplay();
	DisplayMetrics metrics = new DisplayMetrics();
	display.getMetrics(metrics);
	return metrics.widthPixels;

    }

    @Override
    public void bannerDidLoad(View adView,int preferredHeightDP) {

	mMNGAdView = adView;

	if (mAutoDisplay) {
	    showBanner();
	}
	mBannerCallBack.success();

    }

    @Override
    public void bannerDidFail(Exception adsException) {
	mBannerCallBack.error(adsException.toString());

    }
   @Override
    public void bannerResize(MNGFrame frame) {
   }

    @Override
    public void interstitialDidLoad() {
	PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
		"DID_LOAD");
	pluginResult.setKeepCallback(true);
	mInterstitialCallBack.sendPluginResult(pluginResult);
	}

    @Override
    public void interstitialDidFail(Exception adsException) {
	mInterstitialCallBack.error(adsException.toString());
    }

    @Override
    public void interstitialDisappear() {
    
	mInterstitialCallBack.success("DID_DISAPPEAR");
    }

    @Override
    public void onMNGAdsSDKFactoryDidFinishInitializing() {

	mInitializationCallBack.success();

    }

    @Override
    public void nativeObjectDidLoad(MNGNativeObject nativeObject) {
	mNativeCallBack.success(getString(nativeObject));
	mMNGNativeObject = nativeObject;

    }

    @Override
    public void nativeObjectDidFail(Exception adsException) {
	mNativeCallBack.error(adsException.toString());

    }
  
}
